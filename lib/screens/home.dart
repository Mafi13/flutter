import 'package:flutter/material.dart';

class Home extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Center(
        child: Text(
          time(),
          textDirection: TextDirection.ltr,
          style: TextStyle(
            color: Colors.red,fontSize: 36,
            fontFamily: "Sahel",
            fontWeight: FontWeight.bold,
        ),
      ),
    ));
  }
String time(){
  String hello;
  DateTime k = new DateTime.now();
  var s = k.hour > 10 ? k.hour.toString(): "0"+k.hour.toString();
  
  hello = k.hour >= 12 ? "کصکش شبه":" کصکش صبه";
  var time = k.minute > 10 ? k.minute.toString() : "0" + k.minute.toString() ;

  var timeAll = time + " : " + s.toString();
  return hello + "    \t \t   "+timeAll;
}
}